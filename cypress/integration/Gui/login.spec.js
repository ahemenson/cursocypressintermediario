/// <reference types="Cypress" /> 

/// a comando acima dá acesso Intelisense do cypress, com isso temos o autocomplete e informações das funcões do cypress


describe('login', () => { // informamos título e função 
  it('Successfully login', () => {
    cy.login();

    cy.get('.qa-user-avatar').should('exist');
  });
});